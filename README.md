# Virtual Workshop Presentations

This project contains `PDF` versions of GitLab virtual workshop slide decks.

# Workshop List

| Date       | Workshop Name | PDF |
| ---------- | ------------- | --- |
| 2021-01-20 | CICD Basics   | [PDF Link](User_Guide_-_Jan_2021_CICD_Basic_Virtual_Workshop_-_West.pdf) | 
| 2022-11-15 | GitHub to GitLab Migration | [PDF Link]() |
